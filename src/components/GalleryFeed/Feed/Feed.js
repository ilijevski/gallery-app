import React from "react";
import Post from "../Post/Post";
import classes from "./Feed.module.css";

const Feed = ({ appPosts }) => {
  return (
    <div className={classes.GalleryFeed}>
      {appPosts
        .filter((post) => post.userName?.length !== 0)
        .map((post, index) => (
          <Post key={index} postData={post}></Post>
        ))}
    </div>
  );
};

export default Feed;

import React from "react";
import { NavLink } from "react-router-dom";
import DarkModeButton from "../../DarkModeButton/DarkModeButton";
import classes from "./Navbar.module.css";

const Navbar = () => {
	return (
		<nav className={classes.Navbar}>
			<ul className={classes.Navigation}>
				<li>
					<NavLink
						exact={true}
						to="/"
						activeClassName={classes.NavbarActiveItem}
					>
						Feed
					</NavLink>
				</li>
				<li>
					<NavLink
						to="/create"
						activeClassName={classes.NavbarActiveItem}
					>
						Create
					</NavLink>
				</li>
			</ul>
			<DarkModeButton />
		</nav>
	);
};

export default Navbar;

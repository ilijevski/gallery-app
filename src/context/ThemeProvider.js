import { useState } from "react";
import { DarkModeContext } from "./DarkModeContext";

const ThemeProvider = (props) => {
	const [theme, setTheme] = useState("light");

	const toggleTheme = () => {
		setTheme(theme === "light" ? "dark" : "light");
	};

	const color = theme === "light" ? "#333" : "#fff";
	const backgroundColor = theme === "light" ? "#fff" : "#333";

	document.body.style.color = color;
	document.body.style.backgroundColor = backgroundColor;

	const contextObj = {
		theme,
		toggleTheme
	}
	return (
		<DarkModeContext.Provider value={contextObj}>
			{props.children}
		</DarkModeContext.Provider>
	);
};

export default ThemeProvider;

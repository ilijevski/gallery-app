import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { DummyData } from "../assets/data/DummyData";
import CreatePost from "../components/CreatePost/CreatePost";
import Feed from "../components/GalleryFeed/Feed/Feed";
import Navbar from "../components/Layout/Navbar/Navbar";
import ThemeProvider from "../context/ThemeProvider";

function App() {
	const [posts, setPosts] = useState(DummyData);

	const onNewPost = (newPost) => {
		setPosts([newPost, ...posts])
	}

	return (
		<ThemeProvider>
			<Router>
				<Navbar />
				<Switch>
					<Route exact path="/">
						<Feed appPosts={posts}></Feed>
					</Route>
					<Route
						exact
						path="/create"
						render={(props) => <CreatePost {...props} onNewCreatedPost={onNewPost}></CreatePost>}>
					</Route>
				</Switch>
			</Router>
		</ThemeProvider>
	);
}

export default App;
